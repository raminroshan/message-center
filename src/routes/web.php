<?php
Route::group(['namespace' => 'Pishgaman\SMS\Http\Controllers', 'middleware' => ['web']], function(){
    Route::prefix('admin')->middleware('auth')->group(function () {
        Route::prefix('message/center')->group(function () {
            Route::match(['get','post'],'sms', 'SMSController@List')->name('pishgaman.sms');
            Route::match(['get','post'],'action', 'SMSController@action')->name('pishgaman.sms.action');
            Route::prefix('units')->group(function () {
                Route::match(['get','post'],'' , 'UnitController@List')->name('MC.Units');
                Route::match(['get','post'],'sms' , 'SMSController@ListUnit')->name('MC.Units.Unit.SMS');
                Route::get('new'  , 'UnitController@new_form')->name('MC.Units.New');
                Route::get('edit' , 'UnitController@edit_form')->name('MC.Units.edit');
                Route::match(['get','post'],'action' , 'UnitController@action')->name('MC.Units.Action');
            });
        });
    });
    Route::match(['get','post'],'send_greet', 'SMSController@send_greet')->name('pishgaman.sms.send_greet');
});
