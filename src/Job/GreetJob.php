<?php

namespace Pishgaman\SMS\Job;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Pishgaman\SMS\SMS;

class GreetJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $phone , $msg;

    public function __construct($phone,$msg)
    {
        $this->phone = $phone;
        $this->msg = $msg;
    }

    public function handle(SMS $sms,Request $request)
    {
        $request->data = array(
            "ParameterArray" => array(
                array(
                    "Parameter" => "VerificationCode",
                    "ParameterValue" => $this->msg
                ),
            ),
            "Mobile" => $this->phone,
            "TemplateId" => config('PishgamanSMS.SMSIR_GreetJob')
        );
        $sms->UltraFastSend($request);
    }
}
