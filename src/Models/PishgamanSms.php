<?php

namespace Pishgaman\SMS\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PishgamanSms extends Model
{
    use SoftDeletes;

    protected $table = 'pishgaman_sms';

    protected $dates = ['deleted_at'];

    protected $fillable = ['messageid','message','status','statustext','sender','sender_id','receptor','receptor_id','cost'];


    // //make new msg
    // public function scopeNewMsg($query,$request)
    // {
    //     $query->create(['mc_unit_id' => $request->mc_unit_id,'to' => $request->to,'type' => $request->type,'msg' => $request->msg]);
    // }

    // //update msg
    // public function scopeUpdateMsg($query,$request)
    // {
    //     $query->where('id', $request->id)->update(['mc_unit_id' => $request->mc_unit_id,'to' => $request->to,'type' => $request->type,'msg' => $request->msg]);
    // }

    // //delete msg
    // public function scopeDeleteMsg($query,$request)
    // {
    //     $query->where('id',$request->id)->delete();
    // }

}
