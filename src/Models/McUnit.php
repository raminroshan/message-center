<?php

namespace Pishgaman\SMS\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class McUnit extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['title','description','status','ceiling','maturity','admin_id'];

    public function McMessageList()
    {
      return $this->hasMany('App\Model\MessageCenter\McMessageList', 'id' , 'mc_unit_id')->withTrashed();
    }

    public function AdminUser()
    {
      return $this->hasone('App\User', 'id' , 'admin_id')->withTrashed();
    }

    //make new unit
    public function scopeNewMsg($query,$request)
    {
        $query->create(['title' => $request->title,'admin_id' => $request->admin_id ?? 0,'description' => $request->description,'status' => $request->status,'ceiling' => $request->ceiling ?? 0,'maturity' => $request->maturity]);
    }

    //update Unit
    public function scopeUpdateMsg($query,$request)
    {
        $query->where('id', $request->id)->update(['title' => $request->title,'admin_id' => $request->admin_id ?? 0,'description' => $request->description,'status' => $request->status,'ceiling' => $request->ceiling ?? 0,'maturity' => $request->maturity]);
    }

    //delete Unit
    public function scopeDeletMsg($query,$request)
    {

        if($request->has('mychk'))
        {
            $request->validate([
                'mychk'            => 'required',
            ],[
                'id.required'       => 'خطا در دریافت شناسه گروه‌ها',
            ]);
            foreach ( $request->mychk as $c)
            {
                $delete = $query->orwhere('id',$c);
            }
            $delete->update(['admin_id' => 0]);
            $delete->delete();
            session()->flash('status', 'گروه ها با موفقیت حذف شد');
        }
        else if($request->has('del'))
        {
            $request->validate([
                'id'            => 'required|integer',
            ],[
                'id.integer'        => 'خطا در دریافت شناسه گروه',
                'id.required'       => 'خطا در دریافت شناسه گروه',
            ]);
            $query->where('id',$request->id)->update(['admin_id' => 0]);
            $query->where('id',$request->id)->delete();
            session()->flash('status', 'گروه با موفقیت حذف شد');
        }
        else
        {
            return redirect()->back()->withErrors(['error'=> 'گروهی انتخاب نشده است'])->withInput();
        }
    }
    public function scopeStatus($query,$request)
    {
        $status = $query->where('id', $request->id)->get()->first();
        session()->flash('status', 'وضعیت گروه ' .  $status->title . ' به روز شد');
        $query->where('id', $request->id)->update(['status' =>($status->status ? 0 : 1)]);
    }
}
