<?php

return [

    'Channel'                 => env('Channel'                ,'SMSIr'),

    'SMSIR_APIMessageSendUrl' => env('SMSIR_APIMessageSendUrl', 'http://RestfulSms.com/api/MessageSend'),
    'SMSIR_ApiTokenUrl'       => env('SMSIR_ApiTokenUrl'      , 'http://RestfulSms.com/api/Token'),
    'SMSIR_APIKey'            => env('SMSIR_APIKey'           , ''),
    'SMSIR_SecretKey'         => env('SMSIR_SecretKey'        , ''),
    'SMSIR_LineNumber'        => env('SMSIR_LineNumber'       , ''),

    'Kavenegar_apikey'        => env('Kavenegar_apikey'       , ''),

];
