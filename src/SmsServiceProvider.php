<?php

namespace Pishgaman\SMS;

use Illuminate\Support\ServiceProvider;
use Pishgaman\SMS\Libraries\Kavenegar;
use Pishgaman\SMS\Libraries\SMSIr;

class SmsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');

        $this->loadViewsFrom(__DIR__.'/resources/views', 'SMSView');

        // Loading and Publishing migration files associated SMS Package.
        $this->loadMigrationsFrom(__DIR__.'/Database/migrations');

        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'SMSLang');

        $this->publishes([
            __DIR__.'/Database/seeds/AdminMenuPishgamanSMSTableSeeder.php' => database_path('seeds/AdminMenuPishgamanSMSTableSeeder.php'),
            __DIR__.'/config/PishgamanSMS.php' => config_path('PishgamanSMS.php'),
            ]);

            // $SMSPanel = json_decode($setting->option);
            switch (config('PishgamanSMS.Channel')) {
                case 'Kavenegar':
                    $lib = Kavenegar::class;
                break;
                case 'SMSIr':
                    $lib = SMSIr::class;
                break;
                default:
                $lib = Kavenegar::class;
            break;
        }
        $this->app->bind(SMS::class , $lib , SMSController::class);
    }
}