@extends('Template.'.$Template.'.app')

@section('content')
<div class="row">
    <div class="col-md-12  z-index-50">
        <div class="panel minimal panel-default ">
            <div class="panel-heading clearfix">
                <div class="panel-title"> {{ trans('SMSLang::sms.statistics') }}</div>
            </div>
            <!-- panel body -->
            <div class="panel-body">
                <div class="row col-with-divider">
                    <div class="col-xs-2 text-center stack-order">
                        <h1 class="no-margins">{{ $McUnit->ceiling ?? 0}}</h1>
                        <small>{{ trans('SMSLang::sms.send_ceiling') }}</small>
                    </div>
                    <div class="col-xs-2 text-center stack-order">
                        <h1 class="no-margins">{{$sms_count ?? 0}}</h1>
                        <small>{{ trans('SMSLang::sms.count send sms') }}</small>
                    </div>
                    <div class="col-xs-8 text-center stack-order">
                        <h1 class="no-margins">{{$McUnit->title ?? 0}}</h1>
                        <small>{{ trans('SMSLang::sms.unit') }}</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<form method="POST" action="">
    @csrf
    <div class="row toolbar" style="width: 99%">
        <div class="col-sm-12 col-md-2 col-lg-2">
            <a class="btn btn-success" data-toggle="modal" data-target="#send_sms"><i class="fa fa-send"></i> {{ trans('SMSLang::sms.send sms') }}</a>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4">
            <input name="search" class="form-control" placeholder="{{__('SMSLang::sms.Search')}}" value="{{$search ?? ''}}">
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <select name="orderby" class="form-control">
                <option value="DESC|messageid"    @if($orderby == 'DESC|messageid') selected @endif>{{ trans('SMSLang::sms.DESC|messageid') }}</option>
                <option value="ASC|messageid"     @if($orderby == 'ASC|messageid') selected @endif>{{__('SMSLang::sms.ASC|messageid')}}</option>
                <option value="DESC|statustext" @if($orderby == 'DESC|statustext') selected @endif>{{__('SMSLang::sms.DESC|statustext')}}</option>
                <option value="ASC|statustext"  @if($orderby == 'ASC|statustext') selected @endif>{{__('SMSLang::sms.ASC|statustext')}}</option>
                <option value="DESC|sender" @if($orderby == 'DESC|sender') selected @endif>{{__('SMSLang::sms.DESC|sender')}}</option>
                <option value="ASC|sender"  @if($orderby == 'ASC|sender') selected @endif>{{__('SMSLang::sms.ASC|sender')}}</option>
                <option value="DESC|receptor" @if($orderby == 'DESC|receptor') selected @endif>{{__('SMSLang::sms.DESC|receptor')}}</option>
                <option value="ASC|receptor"  @if($orderby == 'ASC|receptor') selected @endif>{{__('SMSLang::sms.ASC|receptor')}}</option>
            </select>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-2">
            <select name="page_number" class="form-control">
                <option value="5"   @if($page_number == 5)   selected @endif>5</option>
                <option value="10"  @if($page_number == 10)  selected @endif>10</option>
                <option value="15"  @if($page_number == 15)  selected @endif>15</option>
                <option value="20"  @if($page_number == 20)  selected @endif>20</option>
                <option value="25"  @if($page_number == 25)  selected @endif>25</option>
                <option value="30"  @if($page_number == 30)  selected @endif>30</option>
                <option value="50"  @if($page_number == 50)  selected @endif>50</option>
                <option value="100" @if($page_number == 100) selected @endif>100</option>
                <option value="200" @if($page_number == 200) selected @endif>200</option>
                <option value="500" @if($page_number == 500) selected @endif>500</option>
                <option value="0"   @if($page_number == 0)   selected @endif>{{__('SMSLang::sms.All')}}</option>
            </select>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-1">
            <button type="submit" class="btn btn-default" title="{{__('SMSLang::sms.Search')}}">
                <i class="fa fa-search" style="font-size: 20px;padding: 0 !important; width: 100%;"></i>
            </button>
        </div>
    </div>
</form>
<hr>
<div class="row">
    <div class="col-sm-12 table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr class="blue">
                    <th class="text-center">{{__('SMSLang::sms.messageid')}}</th>
                    <th class="text-center">{{__('SMSLang::sms.message')}}</th>
                    <th class="text-center">{{__('SMSLang::sms.sender')}}</th>
                    <th class="text-center">{{__('SMSLang::sms.receptor')}}</th>
                    <th class="text-center">{{__('SMSLang::sms.statustext')}}</th>
                    <th class="text-center">{{__('SMSLang::sms.cost')}}</th>
                </tr>
            </thead>
            @php $i = $sms->currentPage() * $page_number - $page_number + 1 ; @endphp
            @foreach ($sms as $item)
            <tr class="text-center">
                <td>{{$item->messageid}}</td>
                <td><p title="{{$item->message ?? ''}}">{{$item->message ?? ''}}</p></td>
                <td>{{$item->sender ?? ''}}</td>
                <td>{{$item->receptor ?? ''}}</td>
                <td>{{$item->statustext ?? ''}}</td>
                <td>{{$item->cost ?? ''}}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
<div>
    {{ $sms->appends(request()->except('_token'))->links() }}
</div>
<script>
    function pst(id)
    {
        if(id == 'select')
        {
            document.getElementById("pishgaman_sms_phone").style.display = "none";
            document.getElementById("pishgaman_sms_select").style.display = "block";
        }
        else
        {
            document.getElementById("pishgaman_sms_phone").style.display = "block";
            document.getElementById("pishgaman_sms_select").style.display = "none";
            if(id == 'phone')
            {
                document.getElementById("pishgaman_sms_phone").disabled = false;
            }
            else
            {
                document.getElementById("pishgaman_sms_phone").disabled = true;
            }
        }
    }
</script>
<form method="POST" action="{{route('pishgaman.sms.action')}}">
    @csrf
    <div class="modal fade" id="send_sms">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group md-form">
                                <select name="pishgaman_sms_type" id="pishgaman_sms_type" class="form-control" onchange="pst(this.value)">
                                    <option value="all_user">{{__('SMSLang::sms.all user websit')}}</option>
                                    @php
                                    $mg = App\Model\MembershipGroups::where('id','<>','1')->get();
                                    @endphp
                                    @foreach ($mg as $item)
                                    <option value="{{$item->id ?? ''}}">{{__('SMSLang::sms.access level')}} - {{$item->name ?? ''}}</option>
                                    @endforeach
                                    <option value="phone">{{__('SMSLang::sms.enter phone number')}}</option>
                                    <option value="select">{{__('SMSLang::sms.Select User')}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group md-form">
                                <textarea name="pishgaman_sms_phone" id="pishgaman_sms_phone" class="form-control" disabled placeholder="{{__('SMSLang::sms.pishgaman_sms_phone placholder')}}" style="height: 300px"></textarea>
                                <div id="pishgaman_sms_select" style="display:none;border: 1px solid; height: 300px;overflow: scroll;">
                                    <br>
                                    @foreach (App\user::where('phone','<>',null)->get() as $item)
                                    <div class="form-group md-form" style="padding-right: 5px !important;">
                                        <input type="checkbox" name="user_select[]" value="{{$item->phone ?? ''}}"> {{$item->name ?? ''}}
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group md-form">
                                <textarea name="pishgaman_sms_text" id="pishgaman_sms_text" class="form-control" placeholder="{{__('SMSLang::sms.pishgaman_sms_text placeholder')}}" style="height: 300px"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group md-form">
                                <button name="action" value="send_sms" class="btn btn-success">{{__('SMSLang::sms.send sms')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
