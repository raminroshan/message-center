@extends('Template.'.$Template.'.app')

@section('content')
<form action="{{route('MC.Units.Action')}}" method="POST">
    @csrf
    <input type="hidden" name="id" value="{{$McUnit->id ?? 0}}">
    <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-2">
            <label for="title"> {{ trans('SMSLang::sms.unit title') }} :</label>
        </div>
        <div class="col-sm-12 col-md-8 col-lg-4">
            <div class="md-form form-group ">
                <input type="text" value="{{$McUnit->title ?? ''}}" name="title" id="title" class="form-control" placeholder="{{ trans('SMSLang::sms.unit placeholder') }}">
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-2">
            <label for="status">{{ trans('SMSLang::sms.unit status') }} :</label>
        </div>
        <div class="col-sm-12 col-md-8 col-lg-4">
            <div class="md-form form-group ">
                <select class="form-control" name="status" id="status">
                    <option value="1" @if($McUnit->status == 1) selected @endif>{{ trans('SMSLang::sms.active') }}</option>
                    <option value="0" @if($McUnit->status == 0) selected @endif>{{ trans('SMSLang::sms.inactive') }}</option>
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-2">
            <label for="ceiling">{{ trans('SMSLang::sms.SMS_ceiling') }}:</label>
        </div>
        <div class="col-sm-12 col-md-8 col-lg-4">
            <div class="md-form form-group ">
                <input type="number" value="{{$McUnit->ceiling ?? 0}}" name="ceiling" id="ceiling" class="form-control" placeholder="{{ trans('SMSLang::sms.SMS_ceiling_placeholder') }}">
            </div>
        </div>
        {{-- <div class="col-sm-12 col-md-4 col-lg-2">
            <label for="maturity">تاریخ اعتبار پیامک :</label>
        </div>
        <div class="col-sm-12 col-md-8 col-lg-4">
            <div class="md-form form-group ">
                <input type="number" name="maturity" id="maturity" class="form-control " placeholder="سقف تعدا پیامک مجاز را وارد کنید">
            </div>
        </div> --}}
        <div class="col-sm-12 col-md-4 col-lg-2">
            <label for="ceiling">{{ trans('SMSLang::sms.responsible') }} :</label>
        </div>
        <div class="col-sm-12 col-md-8 col-lg-4">
            <div class="md-form form-group ">
                <div class="input-group">
                    <input name="student_supervisor" id="to_name" readonly class="form-control" placeholder="{{ trans('SMSLang::sms.responsible_choice') }}" value="{{$McUnit->AdminUser->name ?? ''}} {{$McUnit->AdminUser->surname ?? ''}}">
                    <span class="input-group-btn">
                        <button class="btn btn-success" type="button" data-toggle="modal" data-target="#search_teacher">{{ trans('SMSLang::sms.choice') }}</button>
                        <button class="btn btn-danger" type="button" onclick="select_desk_user('','','','desk')">{{ trans('SMSLang::sms.delete') }}</button>
                        <input type="hidden" name="admin_id" id="to" value="{{$McUnit->admin_id ?? ''}}">
                    </span>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-2">
            <label for="description">{{ trans('SMSLang::sms.description') }} : </label>
        </div>
        <div class="col-sm-12 col-md-8 col-lg-4">
            <div class="md-form form-group ">
                <textarea name="description" class="form-control" id="description" placeholder="{{ trans('SMSLang::sms.description_unit_placeholder') }}">{{$McUnit->description ?? ''}}</textarea>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 text-center">
            <button class="btn btn-success" name="action" value="edit">{{ trans('SMSLang::sms.unit_edit') }}</button>
        </div>
    </div>
</form>
<div class="modal" id="search_teacher">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ trans('SMSLang::sms.search_responsible') }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="md-form form-line col-margin-top-10 form-group text-center">
                            <input type="text" class="form-control" placeholder="{{ trans('SMSLang::sms.search_user_name') }}" onkeyup="search_processmaker_users(this.value,'desk')">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div id="search_list" class="md-form form-line col-margin-top-10 form-group text-center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
