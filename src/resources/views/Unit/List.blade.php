@extends('Template.'.$Template.'.app')

@section('content')
<form method="POST" action="">
    @csrf
    <div class="row toolbar" style="width: 99%">
        <div class="col-sm-12 col-md-4 col-lg-3">
            <a class="btn btn-success form-control" href="{{route('MC.Units.New')}}"><i class="fa fa-plus"></i> {{__('SMSLang::sms.new')}}</a>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <input name="search" class="form-control" placeholder="{{__('SMSLang::sms.Search')}}" value="{{$search ?? ''}}">
        </div>
        <div class="col-sm-12 col-md-4 col-lg-2">
            <select name="orderby" class="form-control">
                <option value="DESC|id"    @if($orderby == 'DESC|id') selected @endif>{{__('SMSLang::sms.DESC|id')}}</option>
                <option value="ASC|id"     @if($orderby == 'ASC|id') selected @endif>{{__('SMSLang::sms.ASC|id')}}</option>
                <option value="DESC|title" @if($orderby == 'DESC|title') selected @endif>{{__('SMSLang::sms.DESC|title')}}</option>
                <option value="ASC|title"  @if($orderby == 'ASC|title') selected @endif>{{__('SMSLang::sms.ASC|title')}}</option>
                <option value="DESC|status" @if($orderby == 'DESC|status') selected @endif>{{__('SMSLang::sms.DESC|status')}}</option>
                <option value="ASC|status"  @if($orderby == 'ASC|status') selected @endif>{{__('SMSLang::sms.ASC|status')}}</option>
            </select>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-2">
            <select name="page_number" class="form-control">
                <option value="5"   @if($page_number == 5)   selected @endif>5</option>
                <option value="10"  @if($page_number == 10)  selected @endif>10</option>
                <option value="15"  @if($page_number == 15)  selected @endif>15</option>
                <option value="20"  @if($page_number == 20)  selected @endif>20</option>
                <option value="25"  @if($page_number == 25)  selected @endif>25</option>
                <option value="30"  @if($page_number == 30)  selected @endif>30</option>
                <option value="50"  @if($page_number == 50)  selected @endif>50</option>
                <option value="100" @if($page_number == 100) selected @endif>100</option>
                <option value="200" @if($page_number == 200) selected @endif>200</option>
                <option value="500" @if($page_number == 500) selected @endif>500</option>
                <option value="0"   @if($page_number == 0)   selected @endif>{{__('SMSLang::sms.All')}}</option>
            </select>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-2">
            <button type="submit" class="btn btn-default" title="{{__('SMSLang::sms.Search')}}">
                <i class="fa fa-search" style="font-size: 20px;padding: 0 !important; width: 100%;"></i>
            </button>
            <a href="{{ url()->previous()}}" class="btn btn-primary rounded float-left" title="{{__('SMSLang::sms.Return')}}">
                <i class="fa fa-long-arrow-left" style="font-size: 20px;padding: 0 !important;width: 100%;"></i>
            </a>
        </div>
    </div>
</form>
<hr>
<form action="{{route('MC.Units.Action')}}" method="POST">
    @csrf
    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-danger" name="action" value="delete"><i class="fa fa-trash" style="padding: 0px;"></i> {{__('SMSLang::sms.delete_choice')}}</button>
        </div>
    </div>
    <hr><br>
    <div class="row">
        <div class="col-sm-12 table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr class="blue">
                        <th class="text-center">
                            <input type="checkbox" class="filled-in" id="checkall-toggle" title="{{__('SMSLang::sms.selecte_all')}}" onClick="checking();">
                            <label for="checkall-toggle"></label>
                        </th>
                        <th class="text-center">{{__('SMSLang::sms.TITLE')}}</th>
                        <th class="text-center">{{__('SMSLang::sms.description')}}</th>
                        <th class="text-center">{{__('SMSLang::sms.status')}}</th>
                        <th class="text-center">{{__('SMSLang::sms.sms')}}</th>
                        <th class="text-center">{{__('SMSLang::sms.responsible')}}</th>
                        {{-- <th class="text-center">تاریخ انقضا</th> --}}
                        <th class="text-center">{{__('SMSLang::sms.delete')}}</th>

                    </tr>
                </thead>
                @php $i = $McUnit->currentPage() * $page_number - $page_number + 1 ; @endphp
                @foreach ($McUnit as $item)
                <tr class="text-center">
                    <td>
                        <input type="checkbox" class="filled-in" id="chk{{$item->id}}" value="{{$item->id}}" name="mychk[]">
                        <label for="chk{{$item->id}}">{{$i++}}</label>
                    </td>
                    <td>
                        <a href="{{route('MC.Units.edit')}}?id={{$item->id}}" style="color: blue;">
                            {{$item->title}}
                        </a>
                    </td>
                    <td>{{substr($item->description ?? '' , 0 , 25)}}</td>
                    <td><a href="{{route('MC.Units.Action')}}?action=status&id={{$item->id}}"><i class="fa @if($item->status) fa-check @else fa-close @endif"></i></a></td>
                    <td>
                        {{$item->ceiling ?? 0}}
                    </td>
                    <td>
                        {{$item->AdminUser->name ?? ''}} {{$item->AdminUser->surname ?? ''}}
                    </td>
                    {{-- <td>
                        {{$item->maturity ?? 'تاریخ تنظیم نشده'}}
                    </td> --}}
                    <td>
                        <a href="{{route('MC.Units.Action')}}?action=delete&id={{$item->id}}&del=yes" class="btn-floating btn-small deep-orange waves-effect waves-light">
                            <i class="fa fa-trash red-color"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</form>
<div>
    {{ $McUnit->appends(request()->except('_token'))->links() }}
</div>
@endsection
