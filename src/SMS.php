<?php

namespace Pishgaman\SMS;

use Illuminate\Http\Request;

interface SMS
{
    /*
     * This function is for test SMS Provider. It print name of lib that is active for use.
     */
    public function Test();

    public function Send($request);

    public function status();

    public function UltraFastSend($request);

}
