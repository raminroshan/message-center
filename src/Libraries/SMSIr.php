<?php

namespace Pishgaman\SMS\Libraries;

use Illuminate\Http\Request;
use Pishgaman\SMS\SMS;
use Pishgaman\SMS\Models\PishgamanSms;
use Smsirlaravel;
use Pishgaman\SMS\Libraries\SMSir\VerificationCode;
use Pishgaman\SMS\Libraries\SMSir\SendMessage;
use Pishgaman\SMS\Libraries\SMSir\UltraFastSend;
use Pishgaman\SMS\Models\McUnit;
use App\User;

class SMSIr implements SMS
{
    protected $getAPIMessageSendUrl;
    protected $getApiTokenUrl;
    protected $APIKey;
    protected $SecretKey;
    protected $LineNumber;

    public function __construct()
    {
        $this->getAPIMessageSendUrl = config('PishgamanSMS.SMSIR_APIMessageSendUrl');
        $this->getApiTokenUrl = config('PishgamanSMS.SMSIR_ApiTokenUrl');
        $this->APIKey = config('PishgamanSMS.SMSIR_APIKey');
        $this->SecretKey =config('PishgamanSMS.SMSIR_SecretKey');
        $this->LineNumber = config('PishgamanSMS.SMSIR_LineNumber');
    }

    public function Test()
    {

    }
    public function Send($request)
    {
        try {
            $userm = auth()->user();

            $APIKey = $this->APIKey ;
            $SecretKey = $this->SecretKey ;
            $LineNumber = $this->LineNumber;

            $MobileNumbers = $request->receptor;

            $Messages = array($request->pishgaman_sms_text);

            $SmsIR_SendMessage = new SendMessage($APIKey,$SecretKey,$LineNumber);
            $SendMessage = $SmsIR_SendMessage->SendMessage($MobileNumbers,$Messages);

            if($SendMessage['IsSuccessful'] == true)
            {
                if($SendMessage['Ids']!=null)
                {
                    $McUnit = McUnit::where('admin_id',$userm->id)->get()->first();
                    foreach ($SendMessage['Ids'] as $key => $value) {
                        $PishgamanSms = PishgamanSms::create([
                            'messageid'  => $value['ID'] ?? date('dhis'),
                            'message'    => $request->pishgaman_sms_text ?? 'خطا در ثبت',
                            'status'     => 0,
                            'statustext' => 'ارسال به sms.ir' ?? 'خطا در ثبت',
                            'sender'     => $LineNumber .' sms.ir' ?? 'خطا در ثبت',
                            'sender_id'  => $McUnit->id ?? 0,
                            'receptor'   => $value['MobileNo']?? 'خطا در ثبت',
                            'receptor_id'=> $receptor_id ?? 0,
                            'cost'       => '0',
                        ]);
                    }
                }
            }
        } catch (Exeption $e) {
            return 'Error SendMessage : '.$e->getMessage();
        }
    }

    public function status()
    {

    }

    public function UltraFastSend($request)
    {
        $SmsIR_UltraFastSend = new UltraFastSend($this->APIKey,$this->SecretKey);
        $UltraFastSend = $SmsIR_UltraFastSend->UltraFastSend($request->data);
    }
}
