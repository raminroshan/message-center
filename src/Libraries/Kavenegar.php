<?php

namespace Pishgaman\SMS\Libraries;

use Illuminate\Http\Request;
use Pishgaman\SMS\Models\PishgamanSms;
use Pishgaman\SMS\SMS;
use GuzzleHttp\Client;
use Kavenegar as kave;

class Kavenegar implements SMS
{
    protected $apikey;
    public function __construct()
    {
        $this->apikey = config('PishgamanSMS.Kavenegar_apikey');
    }
    public function Test()
    {
        echo 'Kavenegar';
    }

    public function Send($request)
    {
        try{
            $userm = auth()->user();
            $sender = "10008445";
            $message = $request->pishgaman_sms_text;
            $receptor = $request->receptor;
            $result = kave::Send($sender,$receptor,$message);
            if($result){
                foreach($result as $r){
                    $PishgamanSms = PishgamanSms::create([
                        'messageid'  => $r->messageid ?? date('dhis'),
                        'message'    => $r->message ?? 'خطا در ثبت',
                        'status'     => $r->status ?? 0,
                        'statustext' => $r->statustext ?? 'خطا در ثبت',
                        'sender'     => $r->sender .' کاوه نگار' ?? 'خطا در ثبت',
                        'sender_id'  => $request->sender_id ?? 0,
                        'receptor'   => $r->receptor ?? 'خطا در ثبت',
                        'receptor_id'=> $receptor_id ?? 0,
                        'cost'       => $r->cost ?? '0',
                    ]);
                }
                return $PishgamanSms->messageid;
            }
        }
        catch(\Kavenegar\Exceptions\ApiException $e){
            // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
            return $e->errorMessage();
        }
        catch(\Kavenegar\Exceptions\HttpException $e){
            // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
            return $e->errorMessage();
        }
    }
    public function status()
    {
        $PishgamanSms = PishgamanSms::where([['status','<>',14],['status','<>',10]]);
        if($PishgamanSms->count() != 0)
        {
            $messageid = '';
            $PishgamanSms = $PishgamanSms->paginate(300);
            foreach($PishgamanSms as $item)
            {
                $messageid = $messageid . $item->messageid . ',';
            }
            if(($messageid ?? '') != '')
            {
                $url = 'https://api.kavenegar.com/v1/'.$this->apikey.'/sms/status.json?messageid='.$messageid;
                $json = file_get_contents($url);
                $json = json_decode($json);
                foreach($json->entries as $item)
                {
                    PishgamanSms::where('messageid',$item->messageid)->update(['status'=>$item->status,'statustext'=>$item->statustext]);
                }
            }
        }
    }
}
