<?php
namespace Pishgaman\SMS\Libraries\SMSir;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class UltraFastSend {

    /**
    * gets API Ultra Fast Send Url.
    *
    * @return string Indicates the Url
    */
    protected function getAPIUltraFastSendUrl() {
        return "http://RestfulSms.com/api/UltraFastSend";
    }

    /**
    * gets Api Token Url.
    *
    * @return string Indicates the Url
    */
    protected function getApiTokenUrl(){
        return "http://RestfulSms.com/api/Token";
    }

    /**
    * gets config parameters for sending request.
    *
    * @param string $APIKey API Key
    * @param string $SecretKey Secret Key
    * @return void
    */
    public function __construct($APIKey,$SecretKey){
        $this->APIKey = $APIKey;
        $this->SecretKey = $SecretKey;
    }


    public function UltraFastSend($data) {

        $token = $this->GetToken($this->APIKey, $this->SecretKey);
        if($token != false)
        {
            $postData = $data;

            $url = $this->getAPIUltraFastSendUrl();
            $UltraFastSend = $this->execute($postData, $url, $token);
            if($UltraFastSend['IsSuccessful'])
            {
                return true;
            }
            else
            {
                session()->flash('status_danger', 'خطا در ارسال کد شناسایی');
                return false;
            }
        }
    }

    private function GetToken(){
        $client = new \GuzzleHttp\Client();
        $url = $this->getApiTokenUrl();

        $postData = array(
            'UserApiKey' => $this->APIKey,
            'SecretKey' => $this->SecretKey,
            'System' => 'php_rest_v_1_2'
        );


        $postString = json_encode($postData);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
            ]);
            $response = $client->post($url,
            ['body' => $postString ]
        );
        $response = json_decode($response->getBody(), true);

        if($response['IsSuccessful'])
        {
            $resp = $response['TokenKey'];
        }
        else
        {
            $resp = false;
        }
        return $resp;
    }

    private function execute($postData, $url, $token){

        $postString = json_encode($postData);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json','x-sms-ir-secure-token'=>$token]
            ]);
            $response = $client->post($url,
            ['body' => $postString ]
        );
        $response = json_decode($response->getBody(), true);

        return $response;
    }
}

?>