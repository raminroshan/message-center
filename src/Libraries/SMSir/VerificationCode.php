<?php
namespace Pishgaman\SMS\Libraries\SMSir;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class VerificationCode {

    protected function getAPIVerificationCodeUrl() {
        return "http://RestfulSms.com/api/VerificationCode";
    }

    protected function getApiTokenUrl(){
        return "http://RestfulSms.com/api/Token";
    }

    public function __construct($APIKey,$SecretKey){
        $this->APIKey = $APIKey;
        $this->SecretKey = $SecretKey;
    }

    public function VerificationCode($Code, $MobileNumber) {

        $token = $this->GetToken($this->APIKey, $this->SecretKey);
        if($token != false){
            $postData = array(
                'Code' => $Code,
                'MobileNumber' => $MobileNumber,
            );

            $url = $this->getAPIVerificationCodeUrl();
            $VerificationCode = $this->execute($postData, $url, $token);
            $object = json_decode($VerificationCode);

            if(is_object($object)){
                $array = get_object_vars($object);
                if(is_array($array)){
                    $result = $array['Message'];
                } else {
                    $result = false;
                }
            } else {
                $result = false;
            }

        } else {
            $result = false;
        }
        return $result;
    }

    /**
    * gets token key for all web service requests.
    *
    * @return string Indicates the token key
    */
    private function GetToken(){
        $client = new \GuzzleHttp\Client();
        $url = $this->getApiTokenUrl();

        $postData = array(
            'UserApiKey' => $this->APIKey,
            'SecretKey' => $this->SecretKey,
            'System' => 'php_rest_v_1_2'
        );


        $postString = json_encode($postData);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
            ]);
            $response = $client->post($url,
            ['body' => $postString ]
        );
        $response = json_decode($response->getBody(), true);

        if($response['IsSuccessful'])
        {
            $resp = $response['TokenKey'];
        }
        else
        {
            $resp = false;
        }
        return $resp;
    }

    /**
    * executes the main method.
    *
    * @param postData[] $postData array of json data
    * @param string $url url
    * @param string $token token string
    * @return string Indicates the curl execute result
    */
    private function execute($postData, $url, $token){

        $postString = json_encode($postData);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json','x-sms-ir-secure-token'=>$token]
            ]);
            $response = $client->post($url,
            ['body' => $postString ]
        );
        $response = json_decode($response->getBody(), true);
        dd($response);

        return $result;
    }
}

?>
