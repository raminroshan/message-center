<?php

namespace Pishgaman\SMS\Libraries\SMSir;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class SendMessage {


	protected function getAPIMessageSendUrl() {
		return "http://RestfulSms.com/api/MessageSend";
	}

	protected function getApiTokenUrl(){
		return "http://RestfulSms.com/api/Token";
	}

    public function __construct($APIKey,$SecretKey,$LineNumber){
		$this->APIKey = $APIKey;
		$this->SecretKey = $SecretKey;
		$this->LineNumber = $LineNumber;
    }

	public function SendMessage($MobileNumbers, $Messages, $SendDateTime = '') {

		$token = $this->GetToken($this->APIKey, $this->SecretKey);
		if($token != false){
			$postData = array(
				'Messages' => $Messages,
				'MobileNumbers' => $MobileNumbers,
				'LineNumber' => $this->LineNumber,
				'SendDateTime' => $SendDateTime,
				'CanContinueInCaseOfError' => 'false'
			);

			$url = $this->getAPIMessageSendUrl();
			$SendMessage = $this->execute($postData, $url, $token);
            if($SendMessage['IsSuccessful'])
            {
                return $SendMessage;
            }
            else
            {
                session()->flash('status_danger', 'خطا در ارسال کد شناسایی');
                return false;
            }
        }
	}

    private function GetToken(){
        $client = new \GuzzleHttp\Client();
        $url = $this->getApiTokenUrl();

        $postData = array(
            'UserApiKey' => $this->APIKey,
            'SecretKey' => $this->SecretKey,
            'System' => 'php_rest_v_1_2'
        );


        $postString = json_encode($postData);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
            ]);
            $response = $client->post($url,
            ['body' => $postString ]
        );
        $response = json_decode($response->getBody(), true);

        if($response['IsSuccessful'])
        {
            $resp = $response['TokenKey'];
        }
        else
        {
            $resp = false;
        }
        return $resp;
    }

    private function execute($postData, $url, $token){

        $postString = json_encode($postData);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json','x-sms-ir-secure-token'=>$token]
            ]);
            $response = $client->post($url,
            ['body' => $postString ]
        );
        $response = json_decode($response->getBody(), true);

        return $response;
    }
}

?>
