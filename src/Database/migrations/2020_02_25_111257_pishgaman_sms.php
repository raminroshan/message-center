<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PishgamanSms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pishgaman_sms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('messageid');
            $table->text('message');
            $table->unsignedInteger('status');
            $table->text('statustext');
            $table->string('sender');
            $table->unsignedInteger('sender_id');
            $table->text('receptor');
            $table->unsignedInteger('receptor_id')->nullable();
            $table->integer('cost');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('sender_id')->references('id')->on('users');
            $table->foreign('receptor_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pishgaman_sms');
    }
}
