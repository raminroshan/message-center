<?php

use Illuminate\Database\Seeder;

class AdminMenuPishgamanSMSTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_menus')->insert([
            'id' => '100',
            'name' => 'مرکز پیام - واحدها',
        ]);
        DB::table('admin_menus')->insert([
            'id' => '101',
            'name' => 'مرکز پیام - لیست پیامک ها (مدیر)',
        ]);
        DB::table('admin_menus')->insert([
            'id' => '102',
            'name' => 'مرکز پیام - لیست پیامک ها (کاربران)',
        ]);
    }
}
