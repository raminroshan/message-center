<?php

namespace Pishgaman\SMS\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pishgaman\SMS\Models\PishgamanSms;
use Pishgaman\SMS\Models\McUnit;
use Pishgaman\SMS\Job\SMSStatusJob;
use Pishgaman\SMS\Job\GreetJob;
use Pishgaman\SMS\SMS;
use App\User;

class SMSController extends Controller
{
    public function action(SMS $sms,Request $request)
    {
        $func = $request->action;
        $this->$func($sms,$request);
        return redirect()->back();
    }
    public function send_sms($sms,$request)
    {
        $userm = auth()->user();

        if(McUnit::where('admin_id',$userm->id)->count() == 0 || McUnit::where('admin_id',$userm->id)->get()->first()->status == 0) session()->flash('status_danger', 'شما دسترسی ارسال پیامک را ندارید');
        else
        {
            $res = null;
            $McUnit = McUnit::where('admin_id',$userm->id)->get()->first();
            if($request->pishgaman_sms_type == 'phone')
            $res = explode(PHP_EOL,$request->pishgaman_sms_phone);
            else if($request->pishgaman_sms_type == 'all_user')
            {
                $user = User::where('phone','<>',null)->select('phone')->get();
                foreach ($user as $key => $item) {
                    $res[$key] = $item->phone;
                }
            }
            else if($request->pishgaman_sms_type == 'select')
            {
                $res = $request->user_select ;
            }
            else if(is_numeric($request->pishgaman_sms_type))
            {
                $user = User::where([['access_level',$request->pishgaman_sms_type],['phone','<>',null]])->select('phone')->get();
                foreach ($user as $key => $item) {
                    $res[$key] = $item->phone;
                }
            }
            if($res != null)
            {
                $request->sender_id = $McUnit->id;
                $request->receptor = $res;

                if(($McUnit->ceiling - sizeof($request->receptor)) < 0)
                session()->flash('status_danger', 'شما اعتبار کافی برای ارسال پیامک را ندارید');
                else
                {
                    $result = $sms->Send($request);

                    $McUnit->ceiling = $McUnit->ceiling - sizeof($request->receptor);
                    $McUnit->save();
                    session()->flash('status', 'درخواست ارسال پیامک با موفقیت ارسال شد');

                }
            }
            else
            {
                session()->flash('status_danger', 'مخاطبی وجود ندارد');
            }
        }
    }
    public function List(SMS $sms,Request $request)
    {
        SMSStatusJob::dispatch();
        /***** OrderBy *****/
        if($request->has('orderby'))
        {
            $by = explode('|',$request->orderby);
            $SMS = PishgamanSms::orderBy($by[1],$by[0]);
            $orderby = $request->orderby;
        }
        else
        {
            $SMS = PishgamanSms::orderBy('messageid');
            $orderby = 'DESC|messageid';
        }
        /***** Search for SMS name, SMS description, SMS main name *****/
        if($request->has('search'))
        {
            if($request->search != '')
            {
                $search = $request->search;
                $SMS = $SMS->where('messageid' ,'like',"%". $search ."%");
                $SMS = $SMS->orwhere('message' ,'like',"%". $search ."%");
                $SMS = $SMS->orwhere('sender'  ,'like',"%". $search ."%");
                $SMS = $SMS->orwhere('receptor','like',"%". $search ."%");
            }
        }
        /***** number of pages *****/
        if($request->has('page_number'))
        {
            if($request->page_number == '0')
            {
                $page_number = $SMS->count();
                $pg= 0;
            }
            else
            {
                $pg = $page_number = $request->page_number;
            }
        }
        else
        {
            $pg = $page_number = 20;
        }

        $SMS  = $SMS->paginate($page_number);

        $userm = auth()->user();
        $page = [__('SMSLang::sms.message center'), __('SMSLang::sms.sms list') ];

        return view('SMSView::Message.List',[
            'userm'       => $userm           ,
            'sms'      => $SMS          ,
            'page'        => $page            ,
            'search'      => $request->search ,
            'page_number' => $pg              ,
            'orderby'     => $orderby         ,
            ]);
        }
        public function ListUnit(SMS $sms,Request $request)
        {
            $userm = auth()->user();
            if(McUnit::where('admin_id',$userm->id)->count() == 0 || McUnit::where('admin_id',$userm->id)->get()->first()->status == 0)
            {
                session()->flash('status_danger', 'شما دسترسی لازم را ندارید');
                return redirect()->back();
            }
            else
            {
                SMSStatusJob::dispatch();
                $McUnit = McUnit::where('admin_id',$userm->id)->get()->first();
                $SMS = PishgamanSms::where('sender_id',$McUnit->id);
                /***** OrderBy *****/
                if($request->has('orderby'))
                {
                    $by = explode('|',$request->orderby);
                    $SMS = $SMS->orderBy($by[1],$by[0]);
                    $orderby = $request->orderby;
                }
                else
                {
                    $SMS = $SMS->orderBy('messageid');
                    $orderby = 'DESC|messageid';
                }
                $sms_count = $SMS->count();
                /***** Search for SMS name, SMS description, SMS main name *****/
                if($request->has('search'))
                {
                    if($request->search != '')
                    {
                        $search = $request->search;
                        $SMS = $SMS->where('messageid' ,'like',"%". $search ."%");
                        $SMS = $SMS->orwhere('message' ,'like',"%". $search ."%");
                        $SMS = $SMS->orwhere('sender'  ,'like',"%". $search ."%");
                        $SMS = $SMS->orwhere('receptor','like',"%". $search ."%");
                    }
                }
                /***** number of pages *****/
                if($request->has('page_number'))
                {
                    if($request->page_number == '0')
                    {
                        $page_number = $SMS->count();
                        $pg= 0;
                    }
                    else
                    {
                        $pg = $page_number = $request->page_number;
                    }
                }
                else
                {
                    $pg = $page_number = 20;
                }

                $SMS  = $SMS->paginate($page_number);

                $userm = auth()->user();
                $page = [__('SMSLang::sms.message center'), __('SMSLang::sms.sms list') ];

                return view('SMSView::Message.ListUnit',[
                    'userm'       => $userm           ,
                    'sms'         => $SMS             ,
                    'page'        => $page            ,
                    'search'      => $request->search ,
                    'page_number' => $pg              ,
                    'orderby'     => $orderby         ,
                    'McUnit'      => $McUnit          ,
                    'sms_count'   => $sms_count
                    ]);
                }
            }
            public function send_greet(SMS $sms,Request $request)
            {
                $user = User::select('phone')->get();
                $i=0;
                foreach ($user as $item) {
                    $i++;
                    if(($item->phone ?? '') != '')
                    {
                        GreetJob::dispatch($item->phone,$request->msg);
                    }
                    if($i == sizeof($user)) dd(0);
                }
            }
        }
