<?php

namespace Pishgaman\SMS\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pishgaman\SMS\Models\McUnit;
use Hekmatinasser\Verta\Verta;

class UnitController extends Controller
{
    //show list of MC list
    public function List(Request $request)
    {
        $userm = auth()->user();

        /***** OrderBy *****/
        if($request->has('orderby'))
        {
            $by = explode('|',$request->orderby);

            $McUnit = McUnit::orderBy($by[1],$by[0]);
            $orderby = $request->orderby;
        }
        else
        {
            $McUnit = McUnit::orderBy('id','DESC');
            $orderby = 'DESC|id';
        }
        /***** Search for template name, template description, template main name *****/
        if($request->has('search'))
        {
            if($request->search != '')
            {
                $search = $request->search;
                $McUnit = $McUnit->orwhere([['title' , 'like' , "%". $search ."%"]]);
                $McUnit = $McUnit->orwhere([['description' , 'like' , "%". $search ."%"]]);
            }
        }
        /***** number of pages *****/
        if($request->has('page_number'))
        {
            if($request->page_number == '0')
            {
                $page_number = $McUnit->count();
                $pg= 0;
            }
            else
            {
                $pg = $page_number = $request->page_number;
            }
        }
        else
        {
            $pg = $page_number = 20;
        }

        $McUnit = $McUnit->paginate($page_number);

        $page = ['مرکز پیام', 'لیست' ];
        return view('SMSView::Unit.List',[
            'userm'       => $userm           ,
            'McUnit'      => $McUnit          ,
            'page'        => $page            ,
            'search'      => $request->search ,
            'page_number' => $pg              ,
            'orderby'     => $orderby         ,
        ]);
    }
    //page for make new MC
    public function new_form(Request $request)
    {
        $page = ['مرکز پیام', 'ایجاد مرکز پیام جدید' ];
        $userm = auth()->user();
        return view('SMSView::Unit.New',['userm' => $userm , 'page' => $page ]);
    }
    public function edit_form(Request $request)
    {
        $request->validate([
            'id' => 'required | integer',
        ],[
            'id.required' => 'خطا ! دوباره تلاش کنید',
            'id.integer'  => 'خطا ! دوباره تلاش کنید',
        ]);
        $page = ['مرکز پیام', 'ویرایش مرکز پیام جدید' ];
        $userm = auth()->user();
        $McUnit = McUnit::find($request->id);
        return view('SMSView::Unit.Edit',['userm' => $userm , 'page' => $page , 'McUnit'=>$McUnit]);
    }
    public function action(Request $request)
    {
        if($request->has('action'))
        {
            $FuncName = $request->action;
            $this->$FuncName($request);
        }
        else
            session()->flash('status_danger', 'خطا! دوباره تلاش کنید');

        return redirect()->back();
    }
    public function new($request)
    {
        if($request->has('maturity'))
        {
            $date = explode('/',$request->maturity);
            $date = Verta::getGregorian($date[0],$date[1],$date[2]);
            $request->maturity = $date[0].'-'.$date[1].'-'.$date[2];
        }
        $request->validate([
            'title'         => 'required|unique:mc_units|max:255',
            'admin_id'      => 'nullable|integer|unique:mc_units',
            'description'   => 'max:65534',
            'status'        => 'nullable|integer',
            'ceiling'       => 'nullable|integer',
            'maturity'      => 'nullable|date',
        ],[
            'id.integer'        => 'خطا در دریافت شناسه گروه',
            'id.required'       => 'خطا در دریافت شناسه گروه',
            'admin_id.integer'  => 'خطا در دریافت مسئول گروه',
            'admin_id.required' => 'خطا در دریافت مسئول گروه',
            'title.required'    => 'عنوان گروه الزامی است',
            'title.unique'      => 'عنوان گروه باید منحصر به فرد باشد',
            'title.max'         => 'تعداد حروف عنوان گروه بیشتر از حد مجاز است',
            'description.max'   => 'تعداد حروف توضیحات گروه بیشتر از حد مجاز است',
            'status.integer'    => 'خطا در دریافت وضعیت گروه',
            'ceiling.integer'   => 'اعتبار گروه باید از نوع عدد صحیح باشد',
            'maturity.date'     => 'خطا در دریافت فرمت صحیح تاریخ اعتبار',
        ]);

        McUnit::NewMsg($request);

        session()->flash('status', 'گروه ' . $request->title .' با موفقیت اضافه شد');
    }
    public function edit($request)
    {
        $userm = auth()->user();

        if($request->has('maturity'))
        {
            $date = explode('/',$request->maturity);
            $date = Verta::getGregorian($date[0],$date[1],$date[2]);
            $request->maturity = $date[0].'-'.$date[1].'-'.$date[2];
        }
        $request->validate([
            'id'            => 'required|integer',
            'title'         => 'required|max:255|unique:mc_units,title,'.$request->id,
            'description'   => 'max:65534',
            'status'        => 'nullable|integer',
            'admin_id'      => 'nullable|integer|unique:mc_units,admin_id,'.$request->id,
            'ceiling'       => 'nullable|integer',
            'maturity'      => 'nullable|date',
        ],[
            'id.integer'        => 'خطا در دریافت شناسه گروه',
            'id.required'       => 'خطا در دریافت شناسه گروه',
            'admin_id.integer'  => 'خطا در دریافت مسئول گروه',
            'admin_id.required' => 'خطا در دریافت مسئول گروه',
            'admin_id.unique'   => 'خطا در دریافت مسئول گروه. این مسئول به گروه دیگری نسبت داده شده است',
            'title.required'    => 'عنوان گروه الزامی است',
            'title.unique'      => 'عنوان گروه باید منحصر به فرد باشد',
            'title.max'         => 'تعداد حروف عنوان گروه بیشتر از حد مجاز است',
            'description.max'   => 'تعداد حروف توضیحات گروه بیشتر از حد مجاز است',
            'status.integer'    => 'خطا در دریافت وضعیت گروه',
            'ceiling.integer'   => 'اعتبار گروه باید از نوع عدد صحیح باشد',
            'maturity.date'     => 'خطا در دریافت فرمت صحیح تاریخ اعتبار',
        ]);

        McUnit::UpdateMsg($request);

        session()->flash('status', 'گروه ' . $request->title .' با موفقیت ویرایش شد');
    }
    public function delete($request)
    {
        McUnit::DeletMsg($request);
    }
    public function status($request)
    {
        McUnit::Status($request);
    }
}
