**Message Center Package**

Message Center Package for **Pishgaman Content Management**

**Developer** : Ramin Roshanghiyas (ramin.roshanghiyas@gmail.com)

This package allows you to define units for notification using SMS and email. This package is tailor-made for managing pioneer content.

**Installation**

Install with the following command line : 
`
composer require pishgaman/sms
`

Add Tables to Database
```
php asrtisan migrate
```

If needed, add access level information to the database using the following line

```
php artisan vendor:publish
```

**Do this for the following options**

Pishgaman\SMS\SmsServiceProvider

Kavenegar\Laravel\ServiceProviderLaravel5


```
composer dump-autoload
php artisan db:seed --class=AdminMenuPishgamanSMSTableSeeder
```


**Finally enter the api key of your SMS panel in for Kavehnegar in the published file.**

**route :**

MC.Units = Show groups

MC.Units.Unit.SMS = Show send sms to specific groups

pishgaman.sms = Show all send sms

